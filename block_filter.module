<?php

/**
 * @file
 * Block Filter provides a simple input filter to allow an editor (with permission) to embed any block on the site into the content
 */


/**
 * Implements hook_filter()
 */
function block_filter_filter($op, $delta = 0, $format = 1, $text = '', $cache_id = 0) {
  switch ($op) {
    case 'list' :
      return array(0 => t('Block Filter'));

    case 'no cache' :
      return FALSE;

    case 'description' :
      return t('Provides a method of embedding blocks into content');

    case 'prepare' :
      return $text;

    case 'process' :
      return preg_replace_callback('#\[block:(.+):(.+)]#sU', 'block_filter_embed', $text);

    default :
      return $text;
  }
}


/**
 * Implements hook_filter_tips()
 */
function block_filter_filter_tips($delta, $format, $long = FALSE) {
  switch ($delta) {
    case 0 :  return $long ?
                        t('You may embed blocks using the syntax <code>[block:MODULE:DELTA]</code>, for example: <code>[block:system:0]</code> will provide the <em>Powered By Drupal</em> block.') : 
                        t('You may embed blocks using the syntax <code>[block:MODULE:DELTA]</code>.');
  }
}


/**
 * Filter process callback
 */
function block_filter_embed($match) {
  // We need to know the current user and theme
  global $user;
  global $theme_key;

  // Extract the module and delta from the match
  $module = $match[1];
  $delta = $match[2];


  //Make sure this module implements hook_block
  if (!module_hook($module, 'block')) return '';

  // Initialise the theme
  init_theme();

  //Get the users roles
  $rids = array_keys($user->roles);

  //Run a query to get the block from the DB (this checks the block is enabled for thie current users role)
  $block = db_fetch_object(db_query("SELECT b.* FROM {blocks} b LEFT JOIN {blocks_roles} r ON b.module = r.module AND b.delta = r.delta WHERE b.module = '%s' AND b.delta = '%s' AND b.theme = '%s' AND (r.rid IN (" . db_placeholders($rids) . ") OR r.rid IS NULL)", array_merge(array($module, $delta, $theme_key), $rids)));

  // CODE BORROWED FROM block_list
  // Use the user's block visibility setting, if necessary
  if ($block->custom != 0) {
    if ($user->uid && isset($user->block[$block->module][$block->delta])) {
      $enabled = $user->block[$block->module][$block->delta];
    }
    else {
      $enabled = ($block->custom == 1);
    }
  }
  else {
    $enabled = TRUE;
  }

  //Check the page display stuff
  if ($block->pages) {
    if ($block->visibility < 2) {
      $path = drupal_get_path_alias($_GET['q']);
      $page_match = drupal_match_path($path, $block->pages);
      if ($path != $_GET['q']) {
        $page_match = $page_match || drupal_match_path($_GET['q'], $block->pages);
      }
      $page_match = !($block->visibility xor $page_match);
    }
    else {
      $page_match = drupal_eval($block->pages);
    }
  }
  else {
    $page_match = TRUE;
  }
  $block->enabled = $enabled;
  $block->page_match = $page_match;

  if ($block->enabled && $block->page_match) {
    // Check the current throttle status and see if block should be displayed based on server load.
    if (!($block->throttle && (module_invoke('throttle', 'status') > 0))) {

      if (!count(module_implements('node_grants')) && $_SERVER['REQUEST_METHOD'] == 'GET' && ($cid = _block_get_cache_id($block)) && ($cache = cache_get($cid, 'cache_block'))) {
        $array = $cache->data;
      }
      else {
        $array = module_invoke($block->module, 'block', 'view', $block->delta);
        if (isset($cid)) {
          cache_set($cid, $array, 'cache_block', CACHE_TEMPORARY);
        }
      }

      if (isset($array) && is_array($array)) {
        foreach ($array as $k => $v) {
          $block->$k = $v;
        }
      }
    }

    if (isset($block->content) && $block->content) {
      // Override default block title if a custom display title is present.
      if ($block->title) {
        // Check plain here to allow module generated titles to keep any markup.
        $block->subject = $block->title == '<none>' ? '' : check_plain($block->title);
      }
      if (!isset($block->subject)) {
        $block->subject = '';
      }

      return theme('block', $block);
    }
  }

  return '';
}
